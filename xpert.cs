using System;
using System.IO;

class ReadingFiles
{

    public static void Main()
    {

        try
        {   
            string path = @"C:\Users\dlondono\Documents\EQUNOSBOVINOS.dat";
            string pathTo = @"C:\Users\dlondono\Documents\";
            string myFileName = "";
        
       		string[] readText = File.ReadAllLines(path);
        	foreach (string s in readText)
        		{
            		Console.WriteLine(s);
				    bool typeAnimal= checkRule(s);
                    if(typeAnimal){
                        Console.WriteLine("Bovino");
                        myFileName="Bovinos.txt";
                    }else{
                        Console.WriteLine("Equino");
                        myFileName="Equinos.txt";
                    }
                    writeAnswers(s,pathTo,myFileName);
				    Console.WriteLine(typeAnimal);
        		}
		    // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit. good");
            Console.ReadKey();
		
        }
        catch (IOException e)
        {
            Console.WriteLine("The file could not be read:");
            Console.WriteLine(e.Message);
	        Console.WriteLine("Error. Press any key to exit.");
	        Console.ReadKey();
        }
    }


    public static bool checkRule(string verifyOption) 
    {
       string info = verifyOption;
       string search2 = "b";
       bool yesNo = info.Contains(search2);
       return yesNo;
    }

      public static void  writeAnswers(string toWrite,string pathTo, string genFilename) 
    {
       string filepath= String.Concat(pathTo, genFilename); 

        if (!File.Exists(filepath))
        {
            string createText = toWrite + Environment.NewLine;
            File.WriteAllText(filepath, createText);
        }else{
            string appendText = toWrite + Environment.NewLine;
            File.AppendAllText(filepath, appendText);
        }       
    }

}